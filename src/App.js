import './App.css';
import React, { Component, PureComponent } from 'react';
import ReactDOM from 'react-dom';



//Component created using React.Component//
class HelloWorld extends Component {
  
  render(){
    return <h1>Hello world! - React Component</h1>
  }
}

//Component created using React.PureComponent
class HelloWorldPure extends PureComponent {
  render(){
    return <h1>Hello world! - React Pure Component</h1>
  }
}

//Component that uses React.createElement
class HelloWorldElement extends React.Component {
  render() {
    return React.createElement(
      'h1',
      { className: 'header-title' },
      'Hello world! - React Create Element'
    )
  }
}

//Functional Component//
const App = () => {
  
  return (
    <div id="App">
      <HelloWorld></HelloWorld>
      <HelloWorldPure></HelloWorldPure>
      <HelloWorldElement></HelloWorldElement>
    </div>
  );
}
export default App;
